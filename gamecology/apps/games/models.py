from django.db import models
from accounts.models import UserProfile
# Create your models here.

class GameCategory(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to="games/category/image/")

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('games_gamecategory_detail', (), {'pk': self.pk})
    

class Game(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    category = models.ForeignKey(GameCategory, related_name="games")
    description = models.TextField(blank=True, null=True)
    icon = models.ImageField(upload_to="games/image/", blank=True, null=True)
    user = models.ForeignKey(UserProfile)

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('games_game_detail', (), {'pk': self.pk})

class Score(models.Model):
    game = models.ForeignKey(Game, related_name="scores")
    user = models.ForeignKey(UserProfile, related_name="scores")
    score = models.IntegerField(default=0)

    def __unicode__(self):
        return "%s score of %s on %s" %(self.user, self.game, self.score)

    @models.permalink
    def get_absolute_url(self):
        return ('games_score_detail', (), {'pk': self.pk})

    class Meta:
        unique_together = ("game", "user")


class Achievement(models.Model):
    game = models.ForeignKey(Game, related_name="achievements") 
    name = models.CharField(max_length=200)
    required_score = models.IntegerField()

    def __unicode__(self):
        return "%s on %s" %(self.name, self.game)

    @models.permalink
    def get_absolute_url(self):
        return ('games_achievement_detail', (), {'pk': self.pk})

    class Meta:
        unique_together = ("game", "name")

class UserAchievement(models.Model):
    user = models.ForeignKey(UserProfile, related_name="achievements")
    achievement = models.ForeignKey(Achievement, related_name="users")

    def __unicode__(self):
        return "%s got %s" %(self.user, self.achievement)

    @models.permalink
    def get_absolute_url(self):
        return ('games_userachievement_detail', (), {'pk': self.pk})


class Review(models.Model):
    game = models.ForeignKey(Game, related_name="reviews")
    user = models.ForeignKey(UserProfile, related_name="reviews")
    # need to add rating here
    review = models.TextField(blank=True, null=True)

 

    @models.permalink
    def get_absolute_url(self):
        return ('games_review_detail', (), {'pk': self.pk})

    class Meta:
        unique_together = ("game", "user")

from .signals import *