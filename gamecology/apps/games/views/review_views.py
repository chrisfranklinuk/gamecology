from django.views.generic import ListView, DetailView, CreateView, \
                                 DeleteView, UpdateView


from gamecology.apps.games.models import Review


class ReviewView(object):
    model = Review

    def get_template_names(self):
        """Nest templates within review directory."""
        tpl = super(ReviewView, self).get_template_names()[0]
        app = self.model._meta.app_label
        mdl = 'review'
        self.template_name = tpl.replace(app, '{0}/{1}'.format(app, mdl))
        return [self.template_name]


class ReviewBaseListView(ReviewView):
    paginate_by = 10



class ReviewCreateView(ReviewView, CreateView):
    pass




class ReviewDeleteView(ReviewView, DeleteView):

    def get_success_url(self):
        from django.core.urlresolvers import reverse
        return reverse('games_review_list')


class ReviewDetailView(ReviewView, DetailView):
    pass


class ReviewListView(ReviewBaseListView, ListView):
    pass




class ReviewUpdateView(ReviewView, UpdateView):
    pass





