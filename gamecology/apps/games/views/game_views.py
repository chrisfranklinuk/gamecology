from django.views.generic import ListView, DetailView, CreateView, \
                                 DeleteView, UpdateView


from gamecology.apps.games.models import Game
from actstream import action


class GameView(object):
    model = Game

    def get_template_names(self):
        """Nest templates within game directory."""
        tpl = super(GameView, self).get_template_names()[0]
        app = self.model._meta.app_label
        mdl = 'game'
        self.template_name = tpl.replace(app, '{0}/{1}'.format(app, mdl))
        return [self.template_name]


class GameBaseListView(GameView):
    paginate_by = 10



class GameCreateView(GameView, CreateView):
    pass




class GameDeleteView(GameView, DeleteView):

    def get_success_url(self):
        from django.core.urlresolvers import reverse
        return reverse('games_game_list')


class GameDetailView(GameView, DetailView):
    pass


class GameListView(GameBaseListView, ListView):
    pass




class GameUpdateView(GameView, UpdateView):
    pass





