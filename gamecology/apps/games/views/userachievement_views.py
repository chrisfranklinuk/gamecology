from django.views.generic import ListView, DetailView, CreateView, \
                                 DeleteView, UpdateView


from gamecology.apps.games.models import UserAchievement


class UserAchievementView(object):
    model = UserAchievement

    def get_template_names(self):
        """Nest templates within userachievement directory."""
        tpl = super(UserAchievementView, self).get_template_names()[0]
        app = self.model._meta.app_label
        mdl = 'userachievement'
        self.template_name = tpl.replace(app, '{0}/{1}'.format(app, mdl))
        return [self.template_name]


class UserAchievementBaseListView(UserAchievementView):
    paginate_by = 10



class UserAchievementCreateView(UserAchievementView, CreateView):
    pass




class UserAchievementDeleteView(UserAchievementView, DeleteView):

    def get_success_url(self):
        from django.core.urlresolvers import reverse
        return reverse('games_userachievement_list')


class UserAchievementDetailView(UserAchievementView, DetailView):
    pass


class UserAchievementListView(UserAchievementBaseListView, ListView):
    pass




class UserAchievementUpdateView(UserAchievementView, UpdateView):
    pass





