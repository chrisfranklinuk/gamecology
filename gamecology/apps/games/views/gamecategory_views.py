from django.views.generic import ListView, DetailView, CreateView, \
                                 DeleteView, UpdateView


from gamecology.apps.games.models import GameCategory


class GameCategoryView(object):
    model = GameCategory

    def get_template_names(self):
        """Nest templates within gamecategory directory."""
        tpl = super(GameCategoryView, self).get_template_names()[0]
        app = self.model._meta.app_label
        mdl = 'gamecategory'
        self.template_name = tpl.replace(app, '{0}/{1}'.format(app, mdl))
        return [self.template_name]


class GameCategoryBaseListView(GameCategoryView):
    paginate_by = 10



class GameCategoryCreateView(GameCategoryView, CreateView):
    pass




class GameCategoryDeleteView(GameCategoryView, DeleteView):

    def get_success_url(self):
        from django.core.urlresolvers import reverse
        return reverse('games_gamecategory_list')


class GameCategoryDetailView(GameCategoryView, DetailView):
    pass


class GameCategoryListView(GameCategoryBaseListView, ListView):
    pass




class GameCategoryUpdateView(GameCategoryView, UpdateView):
    pass





