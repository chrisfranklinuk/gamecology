from django.views.generic import ListView, DetailView, CreateView, \
                                 DeleteView, UpdateView


from gamecology.apps.games.models import Achievement


class AchievementView(object):
    model = Achievement

    def get_template_names(self):
        """Nest templates within achievement directory."""
        tpl = super(AchievementView, self).get_template_names()[0]
        app = self.model._meta.app_label
        mdl = 'achievement'
        self.template_name = tpl.replace(app, '{0}/{1}'.format(app, mdl))
        return [self.template_name]


class AchievementBaseListView(AchievementView):
    paginate_by = 10



class AchievementCreateView(AchievementView, CreateView):
    pass




class AchievementDeleteView(AchievementView, DeleteView):

    def get_success_url(self):
        from django.core.urlresolvers import reverse
        return reverse('games_achievement_list')


class AchievementDetailView(AchievementView, DetailView):
    pass


class AchievementListView(AchievementBaseListView, ListView):
    pass




class AchievementUpdateView(AchievementView, UpdateView):
    pass





