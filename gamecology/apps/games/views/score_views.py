from django.views.generic import ListView, DetailView, CreateView, \
                                 DeleteView, UpdateView


from gamecology.apps.games.models import Score


class ScoreView(object):
    model = Score

    def get_template_names(self):
        """Nest templates within score directory."""
        tpl = super(ScoreView, self).get_template_names()[0]
        app = self.model._meta.app_label
        mdl = 'score'
        self.template_name = tpl.replace(app, '{0}/{1}'.format(app, mdl))
        return [self.template_name]


class ScoreBaseListView(ScoreView):
    paginate_by = 10



class ScoreCreateView(ScoreView, CreateView):
    pass




class ScoreDeleteView(ScoreView, DeleteView):

    def get_success_url(self):
        from django.core.urlresolvers import reverse
        return reverse('games_score_list')


class ScoreDetailView(ScoreView, DetailView):
    pass


class ScoreListView(ScoreBaseListView, ListView):
    pass




class ScoreUpdateView(ScoreView, UpdateView):
    pass





