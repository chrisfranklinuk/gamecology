from django.db.models.signals import post_save
from actstream import action
from .models import Game, Review

def game_add(sender, instance, created, **kwargs):
	if created:
		action.send(instance, verb='was added to the game database.')

post_save.connect(game_add, sender=Game)

def review_add(sender, instance, created, **kwargs):
	if created:
		action.send(instance.user, verb='added review', action_object=instance, target=instance.game)
post_save.connect(review_add, sender=Review)