from django.conf.urls import patterns, url



from gamecology.apps.games.views.gamecategory_views import *
urlpatterns = patterns('',

    url(
        regex=r'^gamecategory/create/$',
        view=GameCategoryCreateView.as_view(),
        name='games_gamecategory_create'
    ),


    url(
        regex=r'^gamecategory/(?P<pk>\d+?)/delete/$',
        view=GameCategoryDeleteView.as_view(),
        name='games_gamecategory_delete'
    ),
    url(
        regex=r'^gamecategory/(?P<pk>\d+?)/$',
        view=GameCategoryDetailView.as_view(),
        name='games_gamecategory_detail'
    ),
    url(
        regex=r'^gamecategory/$',
        view=GameCategoryListView.as_view(),
        name='games_gamecategory_list'
    ),


    url(
        regex=r'^gamecategory/(?P<pk>\d+?)/update/$',
        view=GameCategoryUpdateView.as_view(),
        name='games_gamecategory_update'
    ),


)


from gamecology.apps.games.views.game_views import *
urlpatterns += patterns('',

    url(
        regex=r'^game/create/$',
        view=GameCreateView.as_view(),
        name='games_game_create'
    ),


    url(
        regex=r'^game/(?P<pk>\d+?)/delete/$',
        view=GameDeleteView.as_view(),
        name='games_game_delete'
    ),
    url(
        regex=r'^game/(?P<pk>\d+?)/$',
        view=GameDetailView.as_view(),
        name='games_game_detail'
    ),
    url(
        regex=r'^game/$',
        view=GameListView.as_view(),
        name='games_game_list'
    ),


    url(
        regex=r'^game/(?P<pk>\d+?)/update/$',
        view=GameUpdateView.as_view(),
        name='games_game_update'
    ),


)


from gamecology.apps.games.views.score_views import *
urlpatterns += patterns('',

    url(
        regex=r'^score/create/$',
        view=ScoreCreateView.as_view(),
        name='games_score_create'
    ),


    url(
        regex=r'^score/(?P<pk>\d+?)/delete/$',
        view=ScoreDeleteView.as_view(),
        name='games_score_delete'
    ),
    url(
        regex=r'^score/(?P<pk>\d+?)/$',
        view=ScoreDetailView.as_view(),
        name='games_score_detail'
    ),
    url(
        regex=r'^score/$',
        view=ScoreListView.as_view(),
        name='games_score_list'
    ),


    url(
        regex=r'^score/(?P<pk>\d+?)/update/$',
        view=ScoreUpdateView.as_view(),
        name='games_score_update'
    ),


)


from gamecology.apps.games.views.achievement_views import *
urlpatterns += patterns('',

    url(
        regex=r'^achievement/create/$',
        view=AchievementCreateView.as_view(),
        name='games_achievement_create'
    ),


    url(
        regex=r'^achievement/(?P<pk>\d+?)/delete/$',
        view=AchievementDeleteView.as_view(),
        name='games_achievement_delete'
    ),
    url(
        regex=r'^achievement/(?P<pk>\d+?)/$',
        view=AchievementDetailView.as_view(),
        name='games_achievement_detail'
    ),
    url(
        regex=r'^achievement/$',
        view=AchievementListView.as_view(),
        name='games_achievement_list'
    ),


    url(
        regex=r'^achievement/(?P<pk>\d+?)/update/$',
        view=AchievementUpdateView.as_view(),
        name='games_achievement_update'
    ),


)


from gamecology.apps.games.views.userachievement_views import *
urlpatterns += patterns('',

    url(
        regex=r'^userachievement/create/$',
        view=UserAchievementCreateView.as_view(),
        name='games_userachievement_create'
    ),


    url(
        regex=r'^userachievement/(?P<pk>\d+?)/delete/$',
        view=UserAchievementDeleteView.as_view(),
        name='games_userachievement_delete'
    ),
    url(
        regex=r'^userachievement/(?P<pk>\d+?)/$',
        view=UserAchievementDetailView.as_view(),
        name='games_userachievement_detail'
    ),
    url(
        regex=r'^userachievement/$',
        view=UserAchievementListView.as_view(),
        name='games_userachievement_list'
    ),


    url(
        regex=r'^userachievement/(?P<pk>\d+?)/update/$',
        view=UserAchievementUpdateView.as_view(),
        name='games_userachievement_update'
    ),


)


from gamecology.apps.games.views.review_views import *
urlpatterns += patterns('',

    url(
        regex=r'^review/create/$',
        view=ReviewCreateView.as_view(),
        name='games_review_create'
    ),


    url(
        regex=r'^review/(?P<pk>\d+?)/delete/$',
        view=ReviewDeleteView.as_view(),
        name='games_review_delete'
    ),
    url(
        regex=r'^review/(?P<pk>\d+?)/$',
        view=ReviewDetailView.as_view(),
        name='games_review_detail'
    ),
    url(
        regex=r'^review/$',
        view=ReviewListView.as_view(),
        name='games_review_list'
    ),


    url(
        regex=r'^review/(?P<pk>\d+?)/update/$',
        view=ReviewUpdateView.as_view(),
        name='games_review_update'
    ),


)
