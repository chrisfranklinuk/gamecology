from django.contrib import admin
from .models import *

admin.site.register( GameCategory )

admin.site.register( Score )
admin.site.register( Achievement )

admin.site.register( Review )

class ScoreInline(admin.TabularInline):
    """
    Score Image inline
    """
    model = Score
    extra = 0

class AchievementInline(admin.TabularInline):
    """
    Achievement Image inline
    """
    model = Achievement
    extra = 0

class ReviewInline(admin.TabularInline):
    """
    Review Image inline
    """
    model = Review
    extra = 0

class GameAdmin(admin.ModelAdmin):
    inlines = [ ScoreInline, AchievementInline, ReviewInline ]

admin.site.register( Game, GameAdmin )

admin.site.register( UserAchievement )