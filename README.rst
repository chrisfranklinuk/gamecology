.. 

gamecology
======================

Quickstart
----------

To bootstrap the project::

    virtualenv gamecology
    source gamecology/bin/activate
    cd path/to/gamecology/repository
    pip install -r requirements.pip
    pip install -e .
    cp gamecology/settings/local.py.example gamecology/settings/local.py
    manage.py syncdb --migrate



Documentation
-------------

Developer documentation is available in Sphinx format in the docs directory.

Initial installation instructions (including how to build the documentation as
HTML) can be found in docs/install.rst.
