from django.conf.urls import patterns, url, include
from accounts.views import activity_stream
urlpatterns = patterns('',
    (r'^stream/$', activity_stream),
)