from actstream.models import actor_stream

#from django.http import HttpResponse
from django.shortcuts import render_to_response

def activity_stream(request):
    stream = actor_stream(request.user)
    print stream
    return render_to_response('accounts/feed.html', {'stream': stream})